#!/bin/bash
#Script launches transfer script with options for Ariel

run=$1
echo $run

python3 TransferAWSBucket.py s3://igt-genosity-analysis/MiSeq/$run/ s3://genosity-transfer/Client_070118/$run/ --include 'R[1|2]\.fastqc\.*|_R[1|2]\.fastq\.gz' --exclude "Und*"

