#!/usr/bin/env python

from datetime import date, datetime
import subprocess
import sys
import argparse
import logging
import os

#usage: python TransferAWSBucket.py SOURCE_PATH DESTINATTION_BUCKET_PATH [--include 'grep filename pattern' --exclude 'grep sample pattern' --profile aws_profile --dryrun]

#set up transfer log:
today = date.today().strftime("%Y-%m-%d")
script_path = os.path.dirname(os.path.realpath(__file__))
formstr = "%(asctime)s: %(levelname)s: %(message)s"
datestr = "%m/%d/%Y %I:%M:%S %p"
if not os.path.exists(f'{script_path}/transfer_logs/'):
    os.makedirs(f'{script_path}/transfer_logs/')
logging.basicConfig(level = logging.INFO, filename = f"{script_path}/transfer_logs/{datetime.now().strftime('%y%m%d_%H%M%S')}_transfer.log", format = formstr, datefmt = datestr)

#parse arguments:
parser = argparse.ArgumentParser(description = "Transfer files matching pattern from bucket to bucket")
parser.add_argument('source', metavar = 'SourceBucket', type=str, help = 's3://full-source-bucket-path/')
parser.add_argument('destination', metavar = 'DestinationBucketPath', type=str, help = 's3://full-destination-bucket-path/')
parser.add_argument('--include', metavar = 'Include_pattern', help = 'Regex pattern for files to include', default = '_R[1|2]\.fastq\.gz')
parser.add_argument('--exclude', metavar = 'Exclude_pattern', help = 'Regex pattern for files to exclude')
parser.add_argument('--profile', metavar = 'AWS_Profile', default = "default", type=str, help = 'AWS profile (default if excluded)')
parser.add_argument('--dryrun', action = 'store_true', help = 'runs all aws cp comands with --dryrun option')
args = parser.parse_args()

#AWS source, destination, and profile setup:
source_path = args.source
destination_path = args.destination
include_pattern = args.include
exclude_pattern = args.exclude
aws_profile = args.profile
Dryrun = args.dryrun

if source_path.endswith('/') == False:
    source_path += '/'
if destination_path.endswith('/') == False:
    destination_path += '/'


#############################


print("Source path (%s) files to transfer:" % source_path)
#Recursively list files in run, then pipe to grep and awk to find only R[1|2].fastqc. and _R[1|2].fastq.gz files:
aws_s3_ls_cmd = ['aws', 's3', 'ls', '--recursive', '--output', 'text', source_path, '--profile', aws_profile]
#pipe_to_grep = ['grep', '-E', 'R[1|2]\.fastqc\.|_R[1|2]\.fastq\.gz']
pipe_to_grep_exclude = ['grep', '-v', '-E', exclude_pattern]
pipe_to_grep_include = ['grep', '-E', include_pattern]
pipe_to_awk = ['awk', '{print $4}']

bucket_ls = subprocess.Popen(aws_s3_ls_cmd, stdout=subprocess.PIPE)

if type(exclude_pattern) == str:
    bucket_grep_excl = subprocess.Popen(pipe_to_grep_exclude, stdin=bucket_ls.stdout, stdout=subprocess.PIPE)
    bucket_grep_incl = subprocess.Popen(pipe_to_grep_include, stdin=bucket_grep_excl.stdout, stdout=subprocess.PIPE)
else:
    bucket_grep_incl = subprocess.Popen(pipe_to_grep_include, stdin=bucket_ls.stdout, stdout=subprocess.PIPE)

bucket_awk = subprocess.Popen(pipe_to_awk, stdin=bucket_grep_incl.stdout, stdout=subprocess.PIPE)

transfer_list = bucket_awk.stdout.read().splitlines()
transfer_list = [name.decode('utf-8') for name in transfer_list]
#trans_file_dest = [name.decode('utf-8').split('/')[-1] for name in transfer_list]

for f in transfer_list:
    print(f)

source_path_root = f"{'/'.join(source_path.split('/')[0:3])}/"


#Copy files to transfer bucket:
for trans_file in transfer_list:
    aws_cp = ['aws', 's3', 'cp', f"{source_path_root}{trans_file}", f"{destination_path}{trans_file.split('/')[-1]}", 
    '--acl', 'bucket-owner-full-control', '--profile', aws_profile, '--no-progress']
    if Dryrun == True:
        aws_cp.append('--dryrun')
    logging.info(f"aws cp:  {' '.join(aws_cp)}")
    transfer_file = subprocess.run(aws_cp, capture_output = True, check = True)
    logging.info(transfer_file.stdout.decode("utf-8"))

print("Done!")
