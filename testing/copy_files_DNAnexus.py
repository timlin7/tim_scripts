#!/usr/bin/env python2.7
import subprocess


with open('NAT-20-0001_S3_paths.txt') as transfer_list:
    for file_path in transfer_list:
        file_path_strip = file_path.rstrip()

        cmd = ['aws','s3','cp',file_path_strip,'.','--profile','BAA']
        subprocess.call(cmd)
        
        print(cmd)

        file_name = file_path_strip.split('/')[-1]
        
        cmd2 = ['./ua','--auth-token','T6JZgZulAlzGWVjH7bnKJrXQgQqmlWTa','--project','"Pilot Genosity Upload"','--folder','/WES_data/',file_name] 
        subprocess.call(cmd2)
        
        cmd3 = ['rm', file_name]
        subprocess.call(cmd3)


print('Done with transfers')