import json
import psycopg2
import logging
import sys
import rds_config

logger = logging.getLogger()
logger.setLevel(logging.INFO)

rds_host  = rds_config.db_host
rds_username = rds_config.db_username
rds_user_pwd = rds_config.db_password
rds_db_name = rds_config.db_name
rds_port = rds_config.db_port

try:
    print("Attempting to connect to RDS Postgres.")
    
    conn_string = "host='%s' user='%s' password='%s' dbname='%s' port='%s'" % (rds_host, rds_username, rds_user_pwd, rds_db_name, rds_port)
    connection = psycopg2.connect(conn_string)
    

    print("Connected to RDS Postgres.")
except Exception as error:
    print(f'Could not connect to Postgres: {error}')
    logger.error('Error: Could not connect to Postgres')
    sys.exit()

def lambda_handler(event, context):
    print("Attempting to copy data...")
    source_bucket = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']

    print(f'source bucket: {source_bucket}')
    print(f'file_key: {file_key}')

    '''
    with connection:
        cur = connection.cursor()
        #Use postgres aws_s3 extensionto import data
        cur.execute(f"""SELECT aws_s3.table_import_from_s3('batch_stats_summary', '', '(FORMAT csv, NULL ".", HEADER true, 
        DELIMITER E''\t'')', '{source_bucket}', '{file_key}', 'us-east-1')""")
    '''
    
    with connection:
        cur = connection.cursor()
        cur.execute("CREATE TEMP TABLE tmp AS SELECT * FROM batch_stats_summary LIMIT 0")
        cur.execute(f"""SELECT aws_s3.table_import_from_s3('tmp', '', '(FORMAT csv, NULL ".", HEADER true, DELIMITER E''\t'')', '{source_bucket}', 
            '{file_key}', 'us-east-1')""")
        cur.execute("""INSERT INTO batch_stats_summary
                    SELECT tmp.*
                    FROM tmp
                    LEFT JOIN batch_stats_summary USING ("Sample Sequencing Name")
                    WHERE batch_stats_summary."Sample Sequencing Name" is NULL""")
        cur.execute("DROP TABLE tmp")
    
'''
if __name__ == "__main__":
    
    source_bucket = 'rnd-genosity-dev'
    file_key = 'ADX-20-0017_batch_stats/SEQ_CA209-915_Batch_13.10058.10633.batch.statistics.summary_mod.txt'
    print(f'source bucket: {source_bucket}')
    print(f'file_key: {file_key}')
    
    with connection:
        cur = connection.cursor()
        cur.execute("CREATE TEMP TABLE tmp AS SELECT * FROM batch_stats_summary LIMIT 0")
        cur.execute(f"""SELECT aws_s3.table_import_from_s3('tmp', '', '(FORMAT csv, NULL ".", HEADER true, DELIMITER E''\t'')', '{source_bucket}', 
            '{file_key}', 'us-east-1')""")
        cur.execute("""INSERT INTO batch_stats_summary
                    SELECT tmp.*
                    FROM tmp
                    LEFT JOIN batch_stats_summary USING ("Sample Sequencing Name")
                    WHERE batch_stats_summary."Sample Sequencing Name" is NULL""")
        cur.execute("DROP TABLE tmp")
    
    
    with connection:
        cur = connection.cursor()
        #Use postgres aws_s3 extensionto import data
        print("Copying data")
        cur.execute(f"""SELECT aws_s3.table_import_from_s3('batch_stats_summary', '', '(FORMAT csv, NULL ".", HEADER true, DELIMITER E''\t'')', '{source_bucket}', '{file_key}', 'us-east-1')""")
    
'''