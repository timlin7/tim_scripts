import json
import psycopg2
import logging
import os
import sys

logger = logging.getLogger()
logger.setLevel(logging.INFO)

rds_host  = os.environ.get('RDS_HOST')
rds_username = os.environ.get('RDS_USERNAME')
rds_user_pwd = os.environ.get('RDS_USER_PWD')
rds_db_name = os.environ.get('RDS_DB_NAME')



logger.info("Attempting to copy data...")
source_bucket = 'rnd-genosity-dev'
file_key = 'sequencing_run1/example3.batch.statistics.summary.tsv'

logger.info(f'source bucket: {source_bucket}')
logger.info(f'file_key: {file_key}')

try:
    print("Attempting to connect to RDS Postgres.")
    '''
    conn_string = "host='%s' user='%s' password='%s' dbname='%s'" % (
        rds_host, rds_username, rds_user_pwd, rds_db_name)
    connection = psycopg2.connect(conn_string)
    '''
    connection = psycopg2.connect(user='root', host='dmats.cgr2o9eirj9z.us-east-1.rds.amazonaws.com',
        port='5432', password='LUGpFHC9MHVNE;fY79RyqM,i', database='DMAT-dev')
    
    with connection:
        print("Connected to RDS Postgres.")

        cur = connection.cursor()

        #Use postgres aws_s3 extensionto import data
        cur.execute(f"""SELECT aws_s3.table_import_from_s3('import_test_table', '', '(FORMAT csv, HEADER true, DELIMITER E''\t'')', '{source_bucket}', '{file_key}', 'us-east-1')""")

except:
    print('Could not connect to Postgres')
    sys.exit()
