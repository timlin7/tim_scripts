import psycopg2
import sys

#test script to connect to database on local machine and import data from TSV file into existing table

try:
    connection = psycopg2.connect(user = 'postgres', password = 'postgres',
                                  host = 'localhost', port = '5432',
                                  database = 'postgres')

    with connection:
        cur = connection.cursor()
        with open('example2.batch.statistics.summary.tsv', 'r') as input_file:
            next(input_file)  #Skip header row
            cur.copy_from(input_file, 'import_test_table')
        
    connection.commit()

except psycopg2.DatabaseError as error_msg:
    print(f"Error: {error_msg}")
    sys.exit(1)

finally:
    #closing database connection
    if connection:
        cur.close()
        connection.close()
        print("PostgreSQL connection is closed") 
