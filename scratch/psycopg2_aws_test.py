import psycopg2
import sys

#test script to connect to database on local machine and run some commands

try:
    '''
    connection = psycopg2.connect(user = 'root',
                                  host = 'dmats.cgr2o9eirj9z.us-east-1.rds.amazonaws.com',
                                  port = '5432',
                                  database = 'DMAT-dev')
    '''
    connection = psycopg2.connect(user='root', host='dmats.cgr2o9eirj9z.us-east-1.rds.amazonaws.com',
        port='5432', password='LUGpFHC9MHVNE;fY79RyqM,i', database='DMAT-dev')
    
    print('Connected to RDS')
    with connection:
        cur = connection.cursor()
        cur.execute("""SELECT aws_s3.table_import_from_s3('import_test_table', '', '(FORMAT csv, HEADER true, DELIMITER E''\t'')', 'rnd-genosity-dev', 
            'sequencing_run1/example4.batch.statistics.summary.test.tsv', 'us-east-1')""")
    
    '''
    with connection:
        cur = connection.cursor()
        
        #cur.execute("SELECT version()")
        #print(cur.fetchone())

        #cur.execute("CREATE EXTENSION aws_s3 CASCADE")

        #cur.execute("SELECT aws_commons.create_s3_uri('rnd-genosity-dev', 'example.batch.statistics.summary.tsv', 'us-east-1') AS s3_uri")
        #s3_info = cur.fetchone()
        #print(s3_info)


        #Use postgres aws_s3 extensionto import data
        cur.execute("""SELECT aws_s3.table_import_from_s3('import_test_table', '', '(FORMAT csv, HEADER true, DELIMITER E''\t'')', 'rnd-genosity-dev', 
        'sequencing_run1/example.batch.statistics.summary.tsv', 'us-east-1')""")
    '''

except psycopg2.DatabaseError as error_msg:
    print(f"Error: {error_msg}")
    sys.exit(1)

'''
finally:
    #closing database connection
    if connection:
        cur.close()
        connection.close()
        print("PostgreSQL connection is closed") 
'''