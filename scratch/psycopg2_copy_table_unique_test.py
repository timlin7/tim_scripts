import psycopg2
import sys

#test script to connect to AWS Postgres RDS instance. 
#Copy unique results from batch stats summary file inot cumulative batch stats table
#Primary key set to "OrderID" and "Sample Sequencing Name" for now
#Create temporary table and joins based on unique primary key

try:
    '''
    connection = psycopg2.connect(user = 'root',
                                  host = 'dmats.cgr2o9eirj9z.us-east-1.rds.amazonaws.com',
                                  port = '5432',
                                  database = 'DMAT-dev')
    '''
    connection = psycopg2.connect(user='root', host='dmats.cgr2o9eirj9z.us-east-1.rds.amazonaws.com',
        port='5432', password='LUGpFHC9MHVNE;fY79RyqM,i', database='DMAT-dev')
    
    print('Connected to RDS')
    with connection:
        cur = connection.cursor()
        cur.execute("CREATE TEMP TABLE tmp AS SELECT * FROM import_test_table LIMIT 0")
        cur.execute("""SELECT aws_s3.table_import_from_s3('tmp', '', '(FORMAT csv, HEADER true, DELIMITER E''\t'')', 'rnd-genosity-dev', 
            'sequencing_run1/example4.batch.statistics.summary.xtra_col.tsv', 'us-east-1')""")
        cur.execute("""INSERT INTO import_test_table
                    SELECT tmp.*
                    FROM tmp
                    LEFT JOIN import_test_table USING ("Sample Sequencing Name")
                    WHERE import_test_table."Sample Sequencing Name" is NULL""")
        cur.execute("DROP TABLE tmp")
        


except psycopg2.DatabaseError as error_msg:
    print(f"Error: {error_msg}")
    sys.exit(1)

'''
finally:
    #closing database connection
    if connection:
        cur.close()
        connection.close()
        print("PostgreSQL connection is closed") 
'''