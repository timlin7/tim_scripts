#!/usr/bin/env python

from sqlalchemy import create_engine
import pandas as pd

engine = create_engine('postgresql://postgres:postgres@localhost/postgres')

df = pd.read_csv('/Users/timothylin/bitbucket/tim_scripts/example.batch.statistics.summary.tsv', sep = '\t')
df.to_sql('import_test_table', engine, index = False)