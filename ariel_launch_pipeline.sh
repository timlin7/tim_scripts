#!/bin/bash
#run this script after checking/modifying sample sheet on NGS share
#script will sync genosity-output-dev to igt-genosity-outputs, then sync watcher tokens
#bash ariel_launch_pipeline.sh {RUN_FOLDER}

run=$1
echo $run

echo "Sync genosity-output-dev to igt-genosity-outputs
aws s3 sync s3://genosity-output-dev/MiSeq/$run/ s3://igt-genosity-outputs/MiSeq/$run/ --exclude "*sync_status*" --exclude "*seq_status*" --exclude "*pipeline_status*" --exclude "*uploaded_file_list*" --exclude "*tmp*" --exclude "*temp*" --exclude "*Images*" --exclude "*Logs*" --exclude "*Thumbnail_Images*" --exclude "*__*" --delete --acl bucket-owner-full-control --no-progress


echo "Sync watcher tokens to igt-genosity-outputs"
aws s3 sync ~/Downloads/watcher_tokens/ s3://igt-genosity-outputs/MiSeq/$run/ --acl bucket-owner-full-control

