#!/usr/bin/env python
"""This script generates commands to push data to Case Analyzer through Case Analyzer CLI.
Requires SampleSheet, batch name (SEQ_######), and S3 prefix to batch data (s3 path without the bucket)
Extracts Sample Seq Name and RequestIDInstance from SampleSheet.

Output of this script is an .sh file with one loading command per line.  This .sh file will need to be in the same
directory as the CA-CLI binary (ca-cli.jar).

Usage:  python load_CA_cmd.py prefix_to_batch_data batch_name samplesheet.csv --app-prop application.properties
Ex: python load_CA_cmd.py Miseq/210723_A00830_0043_AHFJJMDRXY/batch-2829/ SEQ_123456 SampleSheet.csv
Will need to create application.properties file (defaults to tl-application.properties)
"""



import pandas as pd
import boto3
import botocore
import subprocess
import os
import argparse

script_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(script_path)

#Parse arguments:
parser = argparse.ArgumentParser(description = "Inputs to run CA CLI binary to load file to CA")
parser.add_argument('batch_prefix', metavar='BatchPrefix', type=str, help='S3 prefix/path of batch folder run to be pushed to CA (ex: Miseq/210723_A00830_0043_AHFJJMDRXY/batch-2829/')
parser.add_argument('seqrun', metavar='SequencingRun', type=str, help='Sequencing run number (SEQ_####)')
parser.add_argument('samplesheet', metavar='SampleSheet', type=str, help='Path to sample list containing sample name and RequestIDInstance')
parser.add_argument('--bucket', metavar = 'Bucket', type=str, help = 'source bucket where seq data is stored', default = 'vumc-test-genosity-analysis')
parser.add_argument('--app-prop', metavar = 'ApplicationProperties', type=str, help = 'Path to application properties file', default = f'tl-application.properties')
parser.add_argument('--output', metavar = 'Output', type=str, help = 'Path to output file', default = f'cmd_list.txt')
parser.add_argument('--aws-profile', metavar = 'Profile', type=str, help = 'AWS profile to access bucket', default = f'default')
args = parser.parse_args()

app_prop = args.app_prop
bucket = args.bucket
batch_prefix = args.batch_prefix
seqrun = args.seqrun
samplesheet = args.samplesheet
output = args.output
profile = args.aws_profile

if batch_prefix.endswith('/') == False:
    batch_prefix += '/'

def getFileNames(bucket, batch_prefix, sample, seqrun, app_prop, req_id, output, profile):
    """Find file names from source bucket"""
    prefix = batch_prefix + sample
    file_ext = ['.stats.summary.tsv', '.final.bam', '.final.bai', '.lowCoverageRegions.tsv',
                '.variants.summary.tsv.gz', 'final.vcf.gz', '.final.vcf.gz.tbi']
    file_type = ['stats', 'bam', 'bai', 'lowCovReg', 'var', 'vcf', 'vtbi']
    file_dict = {}

    client = boto3.session.Session(profile_name=profile).client('s3')
    result = client.list_objects_v2(Bucket=bucket, Prefix=prefix, Delimiter='/')
    
    try: 
        result.get('CommonPrefixes')
        for p in result.get('CommonPrefixes'):
            bpath = p.get('Prefix')

        result2 = client.list_objects_v2(Bucket=bucket, Prefix=bpath, Delimiter='/')
        for f in result2['Contents']:
            for ext, t in zip(file_ext, file_type):
                if f['Key'].endswith(ext):
                    file_dict[t] = '/'.join(f['Key'].split('/')[-2:])

        #print(file_dict)
        launch_CA_manual_load(bucket, batch_prefix, sample, file_dict, seqrun, app_prop, req_id, output)
    except:
        print(f'No common prefixes for {sample}. Skipping this sample.')
    return


def launch_CA_manual_load(bucket, batch_prefix, sample, file_dict, seqrun, app_prop, req_id, output):
    """Writes CA CLI java command to text file"""
    cmd = f"java -jar ca-cli.jar --spring.config.location={app_prop} -X run-job --Path s3://{bucket}/{batch_prefix} --SeqRunID {seqrun} "  \
          f"--SampleSeqName {sample} --RequestIDInstance {req_id} --Stats {file_dict['stats']} --BAM {file_dict['bam']} --BAI {file_dict['bai']} " \
          f"--LowCoverage {file_dict['lowCovReg']} --Variants {file_dict['var']} --VCF {file_dict['vcf']} --TBI {file_dict['vtbi']}"
    '''
    try:
        ca_transfer = subprocess.run(cmd, shell = True)
    except:
        print(ca_transfer)
    '''
    
    with open(output, 'a+') as outfile:
        outfile.write(cmd + '\n')
    return

def readSampleSheet(samplesheet):
    df = pd.read_csv(samplesheet, skiprows=23, usecols=['Sample_ID', 'Description'])
    df['Description'] = df['Description'].str.replace('"', '')
    df['RequestID'] = df['Description'].str.extract(r'((?<=RequestIDInstance:).*(?=\|Fingerprint))')
    df['Sample'] = df['Sample_ID'].str.extract(r'(^.*?(?=_D1_))')
    df.drop('Description', axis=1, inplace = True)
    #print(df)
    return df


def main():
    if os.path.exists(output) == True:
        os.remove(output)
    df = readSampleSheet(samplesheet)

    
    for idx, row in df.iterrows():
        sample = row['Sample_ID']
        req_id = row['RequestID']
        getFileNames(bucket, batch_prefix, sample, seqrun, app_prop, req_id, output, profile)
    
if __name__ == '__main__':
    main()



'''
with open('result2.json', 'w') as outfile:
    json.dump(result, outfile, default=str)
'''
# with open('result.json', 'w') as outfile:
#    json.dump(result, outfile, default=str)


'''
s3 = boto3.client("s3")
response = s3.list_objects_v2(
        Bucket=bucket,
        Prefix =prefix,
        MaxKeys=100)
'''

