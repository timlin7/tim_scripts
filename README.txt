This folder contains a few scripts in use:

TransferAWSBucket.py - Looks up files recursive on S3 Source path and transfers to Destination path.
Usage: python TransferAWSBucket.py SOURCE_PATH DESTINATTION_BUCKET_PATH [--include 'grep filename pattern' --exclude 'grep sample pattern' --profile aws_profile --dryrun]

manual_CA_load/load_CA_cmd.py - Outputs commands used to load Case Analyzer through CA CLI (ca-cli.jar)
python load_CA_cmd.py prefix_to_batch_data batch_name samplesheet.csv --app-prop application.properties
Note applications.properites file will need to be created prior to launching run:
(https://genosity.atlassian.net/wiki/spaces/IGT/pages/2031419446/Manual+Data+Loading+-+Guide+for+use)

ariel_data_transfer.sh - transfers Ariel sequencing run by launching TransferAWSBucket.py script
Usage:  ariel_data_transfer.sh {SeqRunFolder}
Needs to be in the same folder as TransferAWSBucket.py

C2i_checksums_mod.R - creates checksum file on transfer bucket after fastq files have been transferred.
To use, open in RStudio and update Run Folder.